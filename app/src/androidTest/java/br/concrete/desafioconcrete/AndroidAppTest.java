package br.concrete.desafioconcrete;

import android.support.v7.widget.RecyclerView;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

import java.util.List;

import br.concrete.desafioconcrete.apigitconcrete.GitRepositoriesService;
import br.concrete.desafioconcrete.model.GitMySelf;
import br.concrete.desafioconcrete.model.GitPullRequests;
import br.concrete.desafioconcrete.model.Item;
import br.concrete.desafioconcrete.views.activities.PullRequestsActivity;
import br.concrete.desafioconcrete.views.components.ProfilePopupDetail;
import br.concrete.desafioconcrete.views.components.RecyclerViewEndlessScroll;
import br.concrete.desafioconcrete.views.fragments.RepositoriesFragment;

/**
 * Created by rafael.alves on 17/01/2018.
 */

public class AndroidAppTest extends
        ActivityInstrumentationTestCase2<MainActivity> {

    private Solo solo;

    public AndroidAppTest() {
        super(MainActivity.class);
    }

    public void setUp() throws Exception {
        solo = new Solo(getInstrumentation(), getActivity());
    }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
    }

    public void testExecuteTests() {
        caseTest1();
    }

    public void caseTest1() {

        // Verifica se a activity princial foi carregada com sucesso;
        solo.assertCurrentActivity("MainActivity", MainActivity.class);
        assertTrue(solo.getCurrentActivity() instanceof MainActivity);
        caseTest2();
    }

    private void caseTest2() {
        /// Valida se conseguiu recuperar a lista de repositorios do git hub
        solo.assertCurrentActivity("MainActivity", MainActivity.class);
        solo.waitForView(R.id.text_view_repository_name);
        List<Item> items = getItems();
        assertTrue(items.size() > 0);
        caseTest3();
    }

    public void caseTest3() {
        /// Valida se a instancia do GitRepositoriesService foi criada com sucesso ;
        GitRepositoriesService instance = GitRepositoriesService.getInstance();
        assertNotNull(instance);
        caseTest5();
    }

    public void caseTest5() {
        /// Valida se a lista de pull request foi carrega com sucesso ;
        solo.scrollUpRecyclerView(0);
        solo.clickInRecyclerView(0);
        solo.waitForActivity(PullRequestsActivity.class);
        solo.waitForView(R.id.text_view_date_pr);
        solo.sleep(2000);

        PullRequestsActivity pullRequestsActivity = (PullRequestsActivity) solo.getCurrentActivity();
        List<GitPullRequests> gitPullRequests = pullRequestsActivity.getCurrentGitPullRequests();
        assertNotNull(gitPullRequests);
        assertTrue(gitPullRequests.size() > 0);

        solo.clickOnScreen(50, 50);
        solo.sleep(1000);

        caseTest6();
    }

    private void caseTest6() {
        // Verifica se abriu o menu corretamente
        solo.clickOnScreen(50, 50);
        solo.sleep(1000);

        String profileMenuText = getActivity().getString(R.string.nav_profile_title);

        solo.clickOnMenuItem(profileMenuText);

        assertTrue(solo.waitForText(profileMenuText));
        caseTest7();
    }

    private void caseTest7() {
        // Verifica se o nome do perfil está correto
        MainActivity mainActivity = getMainActivity();
        GitMySelf gitMySelf = mainActivity.getCurrentGitMySelf();
        assertNotNull(gitMySelf);
        assertTrue(solo.waitForText("Rafael Henrique"));
        caseTest8();
    }

    private void caseTest8() {
        // Verifica se após rolar o barra de menu, os items de repositórios foram acrescentado correntamente;
        solo.clickOnView(solo.getView(R.id.close_popup));
        solo.sleep(1000);

        int itemSize = getItems().size();

        final RecyclerViewEndlessScroll recyclerViewEndlessScroll = (RecyclerViewEndlessScroll) solo.getView(R.id.recycle_list_view);
        recyclerViewEndlessScroll.scrollToPosition(itemSize - 1, getMainActivity());
        assertNotNull(recyclerViewEndlessScroll.getEndlessScrollListener());
    }

    private List<Item> getItems() {
        MainActivity mainActivity = getMainActivity();
        RepositoriesFragment repositoriesFragment = (RepositoriesFragment) mainActivity.getCurrentFragment();
        return repositoriesFragment.getCurrentGitRepositories();
    }

    private MainActivity getMainActivity() {
        return (MainActivity) solo.getCurrentActivity();
    }

}
