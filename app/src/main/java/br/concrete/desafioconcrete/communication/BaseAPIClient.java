package br.concrete.desafioconcrete.communication;

import android.util.Log;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rafael.alves on 16/01/2018.
 */

public class BaseAPIClient {

    protected static volatile Retrofit retrofit = null;

    public BaseAPIClient() {
        initClient();
    }

    private void initClient() {

        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
            interceptor.setLevel
                    (HttpLoggingInterceptor.Level.BODY);
            interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);

            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.github.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
    }

    protected <T> void execute(Call<T> call, final APIClientResponseListener<T> listener) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                Log.d("TAG", response.code() + "");
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                call.cancel();
                listener.onError(call, t);
            }
        });
    }
}
