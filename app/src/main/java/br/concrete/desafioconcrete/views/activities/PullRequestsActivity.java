package br.concrete.desafioconcrete.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.io.Serializable;
import java.util.List;

import br.concrete.desafioconcrete.R;
import br.concrete.desafioconcrete.apigitconcrete.GitRepositoriesService;
import br.concrete.desafioconcrete.communication.APIClientResponseListener;
import br.concrete.desafioconcrete.model.GitPullRequests;
import br.concrete.desafioconcrete.views.adapter.GitPullRequestsAdapter;
import br.concrete.desafioconcrete.views.components.RecyclerViewEndlessScroll;
import retrofit2.Call;

/**
 * Created by rafael.alves on 16/01/2018.
 */

public class PullRequestsActivity extends BaseActivity {

    private RecyclerViewEndlessScroll recycleListView;
    private ProgressBar progressBar;
    private GitPullRequestsAdapter adapter;
    private List<GitPullRequests> currentGitPullRequests;

    @Override
    protected int getLayoutId() {
        return R.layout.pull_requests_fragment;
    }

    @Override
    protected void myOnCreate(@Nullable Bundle savedInstanceState) {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recycleListView = findViewById(R.id.recycle_list_view);
        progressBar = findViewById(R.id.progress_bar);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(PullRequestsActivity.this);
        recycleListView.setLayoutManager(mLayoutManager);

        if (savedInstanceState != null) {
            currentGitPullRequests = (List<GitPullRequests>) savedInstanceState.getSerializable("currentGitPullRequests");
            setAdapter(currentGitPullRequests);
            return;
        }

        Bundle bundle = getIntent().getExtras();

        String user = "";
        String repositoryName = "";

        if (bundle != null) {

            user = bundle.getString("user");
            repositoryName = bundle.getString("repositoryName");
            setTitleActionBar(repositoryName);

            GitRepositoriesService.getInstance().getPullRequests(user, repositoryName, new APIClientResponseListener<List<GitPullRequests>>() {
                @Override
                public void onSuccess(List<GitPullRequests> obj) {
                    setAdapter(obj);
                }

                @Override
                public void onError(Call<List<GitPullRequests>> obj, Throwable t) {
                    showServerError();
                }
            });
        }
    }

    private void setAdapter(List<GitPullRequests> gitPullRequests) {
        if (adapter == null && gitPullRequests != null) {
            adapter = new GitPullRequestsAdapter(gitPullRequests, this);
            recycleListView.setAdapter(adapter);
            progressBar.setVisibility(View.INVISIBLE);
            currentGitPullRequests = gitPullRequests;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("currentGitPullRequests", (Serializable) currentGitPullRequests);
    }

    public List<GitPullRequests> getCurrentGitPullRequests() {
        return currentGitPullRequests;
    }
}
