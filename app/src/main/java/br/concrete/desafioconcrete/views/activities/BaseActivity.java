package br.concrete.desafioconcrete.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import br.concrete.desafioconcrete.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rafael.alves on 16/01/2018.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected abstract int getLayoutId();

    protected abstract void myOnCreate(@Nullable Bundle savedInstanceState);

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        myOnCreate(savedInstanceState);
    }

    public void showServerError() {
        Snackbar.make(toolbar, getString(R.string.server_error_message), Snackbar.LENGTH_LONG).show();
    }

    protected void setTitleActionBar(String title) {
        getSupportActionBar().setTitle(title);
    }
}
