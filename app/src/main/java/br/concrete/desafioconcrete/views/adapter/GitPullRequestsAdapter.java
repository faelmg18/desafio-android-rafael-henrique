package br.concrete.desafioconcrete.views.adapter;

import android.content.Context;
import android.view.View;

import java.util.List;

import br.concrete.desafioconcrete.R;
import br.concrete.desafioconcrete.model.GitPullRequests;
import br.concrete.desafioconcrete.views.adapter.viewholders.GitPullRequestsViewHolder;

/**
 * Created by rafael.alves on 16/01/2018.
 */

public class GitPullRequestsAdapter extends BaseRecyclerViewAdapter<GitPullRequests, GitPullRequestsViewHolder> {

    public GitPullRequestsAdapter(List<GitPullRequests> dataList, Context context) {
        super(dataList, context);
    }

    @Override
    protected GitPullRequestsViewHolder myOnCreateViewHolder(View parent, int viewType) {
        return new GitPullRequestsViewHolder(parent);
    }

    @Override
    protected void myOnBindViewHolder(GitPullRequestsViewHolder holder, int position, GitPullRequests item) {
        holder.bind(item);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.git_pull_requests_item_adapter;
    }
}
