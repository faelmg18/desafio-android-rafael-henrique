package br.concrete.desafioconcrete.views.adapter;

import android.content.Context;
import android.view.View;

import java.util.List;

import br.concrete.desafioconcrete.R;
import br.concrete.desafioconcrete.model.Item;
import br.concrete.desafioconcrete.views.adapter.viewholders.GitRepositoriesViewHolder;

/**
 * Created by rafael.alves on 16/01/2018.
 */

public class GitRepositoriesAdapter extends BaseRecyclerViewAdapter<Item, GitRepositoriesViewHolder> {

    public GitRepositoriesAdapter(List<Item> dataList, Context context) {
        super(dataList, context);
    }

    @Override
    protected GitRepositoriesViewHolder myOnCreateViewHolder(View parent, int viewType) {
        return new GitRepositoriesViewHolder(parent);
    }

    @Override
    protected void myOnBindViewHolder(GitRepositoriesViewHolder holder, int position, final Item item) {
        holder.bind(item);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.git_repository_item_adapter;
    }
}
