package br.concrete.desafioconcrete.views.components;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

import br.concrete.desafioconcrete.R;
import br.concrete.desafioconcrete.views.components.animations.DragAnimations;

/**
 * Created by rafael.alves on 24/11/2017.
 */

public abstract class PopupWindowDialog extends AppCompatActivity {

    protected PopupWindow popWindow;
    protected int mDeviceHeight;
    protected Display display;
    protected Point size;
    protected View inflatedView;
    private Context context;
    private Object obj;
    private int minHeiht;
    private boolean scrolledToUp = false;

    abstract int getLayoutId();

    protected int getBackgroundDrawableId() {
        return R.drawable.popup_bg;
    }

    abstract View onCreatedDialog(View inflatedView);

    protected void onCreate() {
    }

    public PopupWindowDialog(Context context) {
        this.context = context;
        createPopup(context);
    }

    public PopupWindowDialog(Context context, Object obj) {
        this.context = context;
        this.obj = obj;
        createPopup(context);
    }

    public Context getContext() {
        return context;
    }

    // call this method when required to show popup
    private void createPopup(Context context) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        inflatedView = layoutInflater.inflate(getLayoutId(), null, false);

        onCreate();

        // get device size
        display = ((Activity) context).getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        mDeviceHeight = size.y;

        // set height depends on the device size
        popWindow = createPopWindow(inflatedView);
        // set a background drawable with rounders corners
        popWindow.setBackgroundDrawable(context.getResources().getDrawable(getBackgroundDrawableId()));
        // make it focusable to show the keyboard to enter in `EditText`
        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
        popWindow.setOutsideTouchable(true);

        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        setAnimationPopup();

        inflatedView = onCreatedDialog(inflatedView);

    }

    protected void addDragAnimation() {
        DragAnimations dragAnimations = new DragAnimations();
        minHeiht = popWindow.getHeight();

        dragAnimations.addDragAnimation(inflatedView, new DragAnimations.DragAnimationListener() {
            @Override
            public void onDrag(int x, int y) {
                //resize PopWindow
                String value = "0." + String.valueOf(y).substring(0, 2);

                if (y < (minHeiht - 100) && scrolledToUp) {
                    inflatedView.setAlpha(Float.parseFloat(value));
                    scrolledToUp = false;
                    return;
                }

                if (y > minHeiht) {
                    scrolledToUp = true;
                    inflatedView.setAlpha(1);
                    update(x, y);
                }
            }

            @Override
            public void onDragFinish(int x, int y) {
                if (y > minHeiht) {
                    closePopup();
                } else {
                    update(x, minHeiht);
                }
            }
        }, (Activity) context);
    }

    public void update(final int x, final int y) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                popWindow.update(x, y);
            }
        });
    }

    public int getMinHeiht() {
        return minHeiht;
    }

    protected void closePopup() {
        popWindow.dismiss();
    }

    public void setAnimationPopup() {
        popWindow.setAnimationStyle(R.style.PopupAnimation);
    }

    public void showPopup(View parent, int gravity, int x, int y) {
        // show the popup at bottom of the screen and set some margin at bottom ie,
        popWindow.showAtLocation(parent, gravity, x, y);
    }

    public void showPopup(View parent) {
        showPopup(parent, Gravity.BOTTOM, 0, 100);
    }

    protected PopupWindow createPopWindow(View inflatedView) {
        return new PopupWindow(inflatedView, size.x - 10, minHeiht = size.y - 60, true);
    }

    public Object getObj() {
        return obj;
    }
}
