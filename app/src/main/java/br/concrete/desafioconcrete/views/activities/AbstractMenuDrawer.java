package br.concrete.desafioconcrete.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import br.concrete.desafioconcrete.R;
import br.concrete.desafioconcrete.apigitconcrete.GitRepositoriesService;
import br.concrete.desafioconcrete.communication.APIClientResponseListener;
import br.concrete.desafioconcrete.model.GitMySelf;
import br.concrete.desafioconcrete.views.components.ProfilePopupDetail;
import br.concrete.desafioconcrete.views.fragments.RepositoriesFragment;
import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;

/**
 * Created by rafael.alves on 17/01/2018.
 */

public abstract class AbstractMenuDrawer extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    public DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    private ActionBarDrawerToggle toggle;
    private static int oldId;
    public Fragment currentFragment;
    private GitMySelf currentGitMySelf;
    private ProfilePopupDetail profilePopupDetail;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void myOnCreate(@Nullable Bundle savedInstanceState) {

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                navigationView.setCheckedItem(oldId);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState != null) {
            int nav_position_saved = savedInstanceState.getInt("id_nav_item_menu");
            listItemClicked(nav_position_saved);
            currentGitMySelf = (GitMySelf) savedInstanceState.getSerializable("git_my_self");
            setUpMySelfProfileInformation(currentGitMySelf);
            return;
        }

        listItemClicked(R.id.nav_home);

        GitRepositoriesService.getInstance().getGitMySelf(onGetGitMySelf);

    }

    public APIClientResponseListener onGetGitMySelf = new APIClientResponseListener<GitMySelf>() {

        @Override
        public void onSuccess(final GitMySelf gitMySelf) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    currentGitMySelf = gitMySelf;
                    if (gitMySelf != null) {
                        setUpMySelfProfileInformation(gitMySelf);
                    }
                }
            });
        }

        @Override
        public void onError(Call<GitMySelf> obj, Throwable t) {
            showServerError();
        }
    };

    private void setUpMySelfProfileInformation(GitMySelf gitMySelf) {
        View hView = navigationView.getHeaderView(0);
        TextView userName = (TextView) hView.findViewById(R.id.text_view_user_name);
        TextView userLogin = (TextView) hView.findViewById(R.id.text_view_user_login);
        CircleImageView imageViewAvatar = hView.findViewById(R.id.image_view_avatar);

        userName.setText(gitMySelf.name);
        userLogin.setText(gitMySelf.login);
        imageLoader.displayImage(gitMySelf.avatarUrl, imageViewAvatar);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        listItemClicked(id);

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void listItemClicked(int id) {

        if (id == oldId)
            return;

        Fragment fragment = null;
        switch (id) {
            case R.id.nav_home:
                fragment = RepositoriesFragment.newInstance();
                setTitleActionBar(getString(R.string.repositories_title));
                break;

            case R.id.nav_repositories:
                if (currentGitMySelf != null) {
                    profilePopupDetail = new ProfilePopupDetail(this, currentGitMySelf);
                    profilePopupDetail.showPopup(drawer);
                }
                break;

        }

        if (fragment == null) {
            return;
        }

        oldId = id;
        currentFragment = fragment;
        navigationView.setCheckedItem(oldId);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("id_nav_item_menu", oldId);
        outState.putSerializable("git_my_self", currentGitMySelf);
    }

    public GitMySelf getCurrentGitMySelf() {
        return currentGitMySelf;
    }

    public Fragment getCurrentFragment() {
        return currentFragment;
    }

    public ProfilePopupDetail getProfilePopupDetail() {
        return profilePopupDetail;
    }
}
