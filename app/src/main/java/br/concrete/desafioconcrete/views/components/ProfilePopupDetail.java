package br.concrete.desafioconcrete.views.components;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import br.concrete.desafioconcrete.R;
import br.concrete.desafioconcrete.model.GitMySelf;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by rafael.alves on 08/01/2018.
 */

public class ProfilePopupDetail extends PopupWindowDialog {

    private GitMySelf gitMySelf;
    private ImageLoader imageLoader;

    public ProfilePopupDetail(Context context, GitMySelf gitMySelf) {
        super(context, gitMySelf);
    }

    @Override
    int getLayoutId() {
        return R.layout.profile_popup_datail;
    }

    @Override
    protected void onCreate() {
        this.gitMySelf = (GitMySelf) getObj();
    }

    @Override
    View onCreatedDialog(View inflatedView) {
        imageLoader = ImageLoader.getInstance();

        final ImageView closePopupButton = inflatedView.findViewById(R.id.close_popup);
        closePopupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closePopup();
            }
        });

        TextView profileName = inflatedView.findViewById(R.id.profileName);

        TextView profileRegistrationNumber = inflatedView.findViewById(R.id.profileRegistrationNumber);
        TextView textViewNumberOfRepositories = inflatedView.findViewById(R.id.text_view_number_of_repositories);
        TextView textViewNumberOffollowers = inflatedView.findViewById(R.id.text_view_followers);
        CircleImageView circleImageViewProfille = inflatedView.findViewById(R.id.profile_avatar);

        profileName.setText(gitMySelf.name);
        profileRegistrationNumber.setText(gitMySelf.login);
        textViewNumberOfRepositories.setText(gitMySelf.publicRepos.toString());
        textViewNumberOffollowers.setText(gitMySelf.followers.toString());
        imageLoader.displayImage(gitMySelf.avatarUrl, circleImageViewProfille);

        addDragAnimation();

        return inflatedView;
    }

    @Override
    protected PopupWindow createPopWindow(View inflatedView) {
        return new PopupWindow(inflatedView, size.x - 10, size.y / 2 + 60, true);
    }

    @Override
    public void setAnimationPopup() {
        popWindow.setAnimationStyle(R.style.PopupAnimationProfile);
    }
}
