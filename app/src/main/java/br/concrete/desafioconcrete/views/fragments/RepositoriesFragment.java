package br.concrete.desafioconcrete.views.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.io.Serializable;
import java.util.List;

import br.concrete.desafioconcrete.R;
import br.concrete.desafioconcrete.apigitconcrete.GitRepositoriesService;
import br.concrete.desafioconcrete.communication.APIClientResponseListener;
import br.concrete.desafioconcrete.model.GitRepositories;
import br.concrete.desafioconcrete.model.Item;
import br.concrete.desafioconcrete.views.adapter.GitRepositoriesAdapter;
import br.concrete.desafioconcrete.views.components.RecyclerViewEndlessScroll;
import butterknife.BindView;
import retrofit2.Call;

/**
 * Created by rafael.alves on 17/01/2018.
 */

public class RepositoriesFragment extends BaseFragment {

    private static final int GIT_MAX_PAG_REPOSITORIES = 34;

    @BindView(R.id.recycle_list_view)
    RecyclerViewEndlessScroll recycleListView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private GitRepositoriesAdapter adapter;
    private int currentPage = 1;
    private List<Item> currentGitRepositories;

    public static RepositoriesFragment newInstance() {

        Bundle args = new Bundle();

        RepositoriesFragment fragment = new RepositoriesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    int getLayoutId() {
        return R.layout.repositories_fragment;
    }

    @Override
    void myOnCreateFragment(View view, Bundle savedInstanceState) {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycleListView.addEndlessScroll(mLayoutManager, new RecyclerViewEndlessScroll.EndlessScrollListener() {
            @Override
            public void onEndlessScroll() {

                if (currentPage == GIT_MAX_PAG_REPOSITORIES) {
                    recycleListView.setNoMoreItems(true);
                    return;
                }

                currentPage++;
                findGitRepositories();
            }
        });

        if (savedInstanceState != null) {
            currentGitRepositories = (List<Item>) savedInstanceState.getSerializable("currentGitRepositories");
            setAdapter(currentGitRepositories);
            return;
        }

        findGitRepositories();
    }

    private void findGitRepositories() {

        progressBar.setVisibility(View.VISIBLE);
        GitRepositoriesService.getInstance().getRepositories(currentPage, new APIClientResponseListener<GitRepositories>() {
            @Override
            public void onSuccess(final GitRepositories obj) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recycleListView.setLoading(true);
                        if (obj != null) {
                            setAdapter(obj.items);
                        }
                    }
                });
            }

            @Override
            public void onError(Call<GitRepositories> obj, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                showServerError();
            }
        });
    }

    private void setProgressBarEndPage() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        progressBar.setLayoutParams(params);
    }

    private void setAdapter(List<Item> items) {
        progressBar.setVisibility(View.INVISIBLE);
        if (adapter == null && items != null) {
            adapter = new GitRepositoriesAdapter(items, getActivity());
            recycleListView.setAdapter(adapter);
            setProgressBarEndPage();
        } else {
            if (items != null && items.size() > 0) {
                adapter.addAllItem(items);
            }
        }

        currentGitRepositories = adapter.getDataList();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("currentGitRepositories", (Serializable) currentGitRepositories);
    }

    public List<Item> getCurrentGitRepositories() {
        return currentGitRepositories;
    }
}
