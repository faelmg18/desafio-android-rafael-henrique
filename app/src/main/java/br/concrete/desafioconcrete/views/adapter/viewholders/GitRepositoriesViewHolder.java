package br.concrete.desafioconcrete.views.adapter.viewholders;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import br.concrete.desafioconcrete.ApplicationContext;
import br.concrete.desafioconcrete.R;
import br.concrete.desafioconcrete.model.Item;
import br.concrete.desafioconcrete.views.activities.PullRequestsActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by rafael.alves on 16/01/2018.
 */

public class GitRepositoriesViewHolder extends AbstractViewHolder<Item> {

    @BindView(R.id.text_view_repository_name)
    TextView textViewRepositoryName;
    @BindView(R.id.text_view_repository_description)
    TextView textViewRepositoryDescription;
    @BindView(R.id.text_view_user_name)
    TextView textViewUserName;
    @BindView(R.id.text_view_number_of_forks)
    TextView textViewNumberOfForks;
    @BindView(R.id.text_view_number_of_stars)
    TextView textViewNumberOfStars;
    @BindView(R.id.text_view_first_name_and_last_name)
    TextView textViewFirstNameAndLastName;
    @BindView(R.id.profile_image)
    CircleImageView circleImageView;

     ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

    public GitRepositoriesViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(final Item item) {

        textViewRepositoryName.setText(item.getName());
        textViewRepositoryDescription.setText(item.getDescription());
        textViewNumberOfForks.setText(item.forksCount.toString());
        textViewNumberOfStars.setText(item.stargazersCount.toString());
        textViewUserName.setText(item.getOwner().getLogin());
        textViewFirstNameAndLastName.setText(item.getFullName());
        imageLoader.displayImage(item.getOwner().getAvatarUrl(), circleImageView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gotoPullRequestsActivity(item);
            }
        });
    }

    private void gotoPullRequestsActivity(Item item) {
        Intent intent = new Intent(ApplicationContext.getAppContext(), PullRequestsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("user", item.getOwner().getLogin());
        bundle.putString("repositoryName", item.getName());
        intent.putExtras(bundle);
        ApplicationContext.getAppContext().startActivity(intent);
    }
}
