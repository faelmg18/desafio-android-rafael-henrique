package br.concrete.desafioconcrete.views.components.animations;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import br.concrete.desafioconcrete.utils.DisplayManagerUtils;

/**
 * Created by rafael.alves on 17/01/2018.
 */

public class DragAnimations implements View.OnDragListener {
    protected View viewAnimation;
    private FrameLayout.LayoutParams layoutParams;

    public void addDragAnimation(View view, final DragAnimationListener dragAnimationListener, final Activity activity) {

        view.setOnTouchListener(new View.OnTouchListener() {
            int orgX, orgY;
            int offsetX, offsetY;

            int orgWidth, orgHeight;
            int minY = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        if (minY == 0 && ((int) event.getRawY() > minY)) {
                            minY = (int) event.getRawY();
                        }

                        orgX = (int) event.getRawX();
                        orgY = (int) event.getRawY();

                        orgWidth = v.getMeasuredWidth();
                        orgHeight = v.getMeasuredHeight();

                        break;
                    case MotionEvent.ACTION_MOVE:
                        offsetX = 0;
                        offsetY = (int) event.getRawY() - orgY;

                        int diplay = DisplayManagerUtils.getHeight(activity);
                        int rest = DisplayManagerUtils.getHeight(activity) - orgY;
                        Log.i("rest", "" + rest);
                        Log.i("diplay", "" + diplay);
                        Log.i("diplay2", "" + (diplay - (orgHeight + offsetY)));

                        dragAnimationListener.onDrag(orgWidth + offsetX, (diplay - (orgHeight + offsetY)));
                        break;

                    case MotionEvent.ACTION_UP:

                        offsetX = 0;
                        offsetY = (int) event.getRawY() - orgY;
                        dragAnimationListener.onDragFinish(orgWidth + offsetX, orgHeight + offsetY);

                        break;
                }
                return true;
            }
        });
    }

    @Override
    public boolean onDrag(View view, DragEvent dragevent) {
        int action = dragevent.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_LOCATION:
                Log.d("onDrag", "onDrag: ACTION_DRAG_LOCATION");
                //CONTROL POSITION AND SIZE OF DRAGGED VIEW

                //EXAMPLE USING YOUR OLD CODE (previously in ACTION_DRAG_ENDED):
                int x = (int) dragevent.getX();
                int y = (int) dragevent.getY();

                layoutParams.height = x;
                layoutParams.width = y;

                viewAnimation.setLayoutParams(layoutParams);

                View view3 = (View) dragevent.getLocalState();
                ViewGroup owner = (ViewGroup) view3.getParent();
                owner.removeView(view3);
                LinearLayout container = (LinearLayout) viewAnimation;
                container.addView(view3);
                view3.setVisibility(View.VISIBLE);

                break;
            case DragEvent.ACTION_DRAG_STARTED:
                Log.i("onDrag", "onDrag: ACTION_DRAG_STARTED");
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                Log.i("onDrag", "onDrag: ACTION_DRAG_ENTERED");
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                Log.i("onDrag", "onDrag: ACTION_DRAG_EXITED");
                break;
            case DragEvent.ACTION_DROP:
                Log.i("onDrag", "onDrag: ACTION_DROP");
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                Log.i("onDrag", "onDrag: ACTION_DRAG_ENDED");
                break;
            default:
                break;
        }
        return true;
    }

    public interface DragAnimationListener {
        void onDrag(int x, int y);

        void onDragFinish(int x, int y);
    }
}
