package br.concrete.desafioconcrete.views.adapter.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by rafael.alves on 17/01/2018.
 */

public abstract class AbstractViewHolder<T> extends RecyclerView.ViewHolder {

    public abstract void bind(T item);

    public AbstractViewHolder(View itemView) {
        super(itemView);
    }
}
