package br.concrete.desafioconcrete.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.concrete.desafioconcrete.R;
import butterknife.ButterKnife;

/**
 * Created by rafael.alves on 17/01/2018.
 */

public abstract class BaseFragment extends Fragment {
    private View mView;

    abstract int getLayoutId();

    abstract void myOnCreateFragment(View view, Bundle savedInstanceState);

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        mView = inflater.inflate(getLayoutId(),
                container, false);
        ButterKnife.bind(this, mView);
        myOnCreateFragment(mView, savedInstanceState);
        return mView;
    }

    protected void gotoNexScreen(Class<?> aclass) {
        Intent intent = new Intent(getActivity(), aclass);
        startActivity(intent);
    }

    public void showServerError() {
        Snackbar.make(mView, getString(R.string.server_error_message), Snackbar.LENGTH_LONG).show();
    }

    protected View findViewById(int id) {
        return mView.findViewById(id);
    }
}
