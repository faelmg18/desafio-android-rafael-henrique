package br.concrete.desafioconcrete.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;

/**
 * Created by rafael.alves on 17/01/2018.
 */

public class ColorUtil {
    public static int getColor(Context context, int color) {

        try {
            return ContextCompat.getColor(context, color);

        } catch (Exception e) {
            return color;
        }
    }
}
