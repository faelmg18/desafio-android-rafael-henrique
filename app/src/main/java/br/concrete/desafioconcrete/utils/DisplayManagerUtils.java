package br.concrete.desafioconcrete.utils;

import android.app.Activity;
import android.util.DisplayMetrics;

/**
 * Created by rafael.alves on 17/01/2018.
 */

public class DisplayManagerUtils {

    public static int getHeight(Activity activity){
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int height = metrics.heightPixels;
        return  height;
    }

    public static int getWidth(Activity activity){
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        return  width;
    }
}
