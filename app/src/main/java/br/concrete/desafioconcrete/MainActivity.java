package br.concrete.desafioconcrete;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;

import br.concrete.desafioconcrete.views.activities.AbstractMenuDrawer;

public class MainActivity extends AbstractMenuDrawer {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void myOnCreate(@Nullable Bundle savedInstanceState) {
        super.myOnCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
