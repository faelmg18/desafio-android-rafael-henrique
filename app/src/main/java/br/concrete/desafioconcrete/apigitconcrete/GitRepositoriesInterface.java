package br.concrete.desafioconcrete.apigitconcrete;

import java.util.List;

import br.concrete.desafioconcrete.model.GitMySelf;
import br.concrete.desafioconcrete.model.GitPullRequests;
import br.concrete.desafioconcrete.model.GitRepositories;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by rafael.alves on 16/01/2018.
 */

public interface GitRepositoriesInterface {
    /*?q=language:Java&sort=stars*/
    @GET("search/repositories")
    Call<GitRepositories> doGetListRepositoriesModel(@Query("q") String language,
                                                     @Query("sort") String sort,
                                                     @Query("page") int page);

    @GET("repos/{user}/{repository}/pulls?state=all")
    Call<List<GitPullRequests>> listPullRequests(
            @Path("user") String user,
            @Path("repository") String repository);

    @GET("users/faelmg18")
    Call<GitMySelf> doGetGitMySelf();
}
