package br.concrete.desafioconcrete.apigitconcrete;


import java.util.List;

import br.concrete.desafioconcrete.communication.APIClientResponseListener;
import br.concrete.desafioconcrete.communication.BaseAPIClient;
import br.concrete.desafioconcrete.model.GitMySelf;
import br.concrete.desafioconcrete.model.GitPullRequests;
import br.concrete.desafioconcrete.model.GitRepositories;
import retrofit2.Call;

/**
 * Created by rafael.alves on 16/01/2018.
 */

public class GitRepositoriesService extends BaseAPIClient {

    private static final String REPOSITORIES_LANGUAGE = "language:Java";
    private static final String REPOSITORIES_SORT = "stars";
    private static volatile GitRepositoriesService instance;
    private static GitRepositoriesInterface repositoryInterface;

    public GitRepositoriesService() {
        super();
    }

    public static GitRepositoriesService getInstance() {

        if (instance == null) {
            instance = new GitRepositoriesService();
            repositoryInterface = retrofit.create(GitRepositoriesInterface.class);
        }

        return instance;
    }

    public void getRepositories(int page, final APIClientResponseListener responseListener) {

        Call<GitRepositories> call = repositoryInterface
                .doGetListRepositoriesModel(REPOSITORIES_LANGUAGE, REPOSITORIES_SORT, page);

        execute(call, responseListener);
    }

    public void getPullRequests(String user, String repository, final APIClientResponseListener responseListener) {

        Call<List<GitPullRequests>> call = repositoryInterface
                .listPullRequests(user, repository);

        execute(call, responseListener);
    }

    public void getGitMySelf(APIClientResponseListener listener) {
        Call<GitMySelf> call = repositoryInterface.doGetGitMySelf();
        execute(call, listener);
    }
}
